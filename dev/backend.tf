terraform {	
	 backend "s3" {
	   bucket         = "amybucket16oct1"
	   key            = "dev/terraform.tfstate"
	   region         = "ap-south-1"
	   encrypt        = true
	   dynamodb_table = "test"
	 }
	}
